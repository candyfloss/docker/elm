![Docker image with Elm utilities](https://cldup.com/DzkdXc1lZH.png)


# elm

> Docker image with Elm compiler and common utilities

This is a Docker image thought to be used in CI pipelines, with some common Elm
utilities already bundled.

The utilities included are:

  - [elm][]
  - [elm-test][]
  - [elm-format][]
  - [elm-oracle][]
  - [create-elm-app][]
  - [UglifyJS2][]
  - [Simple build script][]

  
[elm]: https://www.npmjs.com/package/elm
[elm-test]: https://github.com/elm-explorations/test
[elm-format]: https://github.com/avh4/elm-format
[elm-oracle]: https://github.com/ElmCast/elm-oracle
[create-elm-app]: https://github.com/halfzebra/create-elm-app
[UglifyJS2]: https://github.com/mishoo/UglifyJS2
[Simple build script]: https://gitlab.com/candyfloss/docker/elm/blob/master/bin/elm-build

